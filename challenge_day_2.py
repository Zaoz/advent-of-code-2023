MAX_CUBES= {"red": 12, "green": 13, "blue": 14}

class Game:
    def __init__(self, line):
        self.bag = {}
        game_header, games = line.split(":")
        self.game_number = int(game_header.split(" ")[1])
        [self.update(part.strip()) for part in games.split(";")]

    def update(self, turns):
        for hand in turns.split(","):
            number, color = hand.strip().split(" ")
            self.bag[color] = max(self.bag.get(color, 0), int(number))

    def possible(self, other: dict) -> bool:
        for color in self.bag.keys():
            if self.bag[color] > other[color]:
                return False
        return True
    
    def powers(self):
        acc = 1
        for val in self.bag.values():
            acc *= val
        return acc

        
if __name__ == "__main__":
    with open("samples/day2.txt", "r") as file:
        text = file.read()
        games = [Game(line) for line in text.split("\n")]
        possible_games = filter(lambda game: game.possible(MAX_CUBES), games)
        game_id_sum = sum([game.game_number for game in possible_games])
        assert game_id_sum == 8
        powers_sum = sum([game.powers() for game in games])
        assert powers_sum == 2286

    with open("inputs/day_2_inputs.txt", "r") as file:
        text = file.read()
        games = [Game(line) for line in text.split("\n")]
        possible_games = filter(lambda game: game.possible(MAX_CUBES), games)
        game_id_sum = sum([game.game_number for game in possible_games])
        print(game_id_sum)
        print(sum([game.powers() for game in games]))
