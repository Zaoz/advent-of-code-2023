import sys

TXT_NUMS = {"one": 1, "two": 2, "three": 3, "four": 4, "five": 5, "six": 6, "seven": 7, "eight": 8, "nine": 9, "zero": 0, "0":0,"1": 1,"2":2, "3": 3, "4": 4, "5": 5, "6":  6, "7": 7, "8": 8, "9": 9}

def find_numbers(line: str) -> int:
    combined_value = find_left(line) + find_right(line)
    return int(combined_value)
   
def find_left(line: str) -> str:
    for char in line:
        if char.isnumeric():
            return char
    return ""

def find_right(line: str) -> str:
    for char in line[::-1]:
        if char.isnumeric():
            return char
    return ""

def find_numbers_better(line: str) -> int:
    return find_left_better(line) + find_right_better(line)

def find_left_better(line: str) -> int:
    pos = sys.maxsize
    val = -1
    for k, v in TXT_NUMS.items():
        if k in line:
            if line.index(k) < pos:
                val = v
                pos = line.index(k)
    print(f"min {val} in {line}")
    return 10 * val

def find_right_better(line: str) -> int:
    pos = -sys.maxsize -1
    val = -1
    for k, v in TXT_NUMS.items():
        if k in line:
            if line.rindex(k) > pos:
                val = v
                pos = line.rindex(k)
    print(f"max {val} in {line}")
    return val

if __name__ == "__main__":
    with open("samples/day1.txt") as file:
        text = file.read()
        total = sum([find_numbers(line) for line in text.split("\n")])
        assert total == 142

    with open("samples/day1pt2.txt") as file:
        text = file.read()
        total2 = sum([find_numbers_better(line) for line in text.split("\n")])
        assert total2 == 281

    with open("inputs/day_1_inputs.txt") as file:
        text = file.read()
        total = sum([find_numbers(line) for line in text.split("\n")])
        print(f"part 1: {total}")
        total2 = sum([find_numbers_better(line) for line in text.split("\n")])
        print(f"part 2: {total2}")

